import numpy as np

def N(i, k, u, t):
    if k == 1:
        if t[i] <= u < t[i+1]:
            return 1.0
        else:
            return 0.0
    else:
        d1 = t[i+k-1] - t[i]
        d2 = t[i+k] - t[i+1]
        
        expr1 = N(i, k-1, u, t) * (u - t[i])
        expr2 = N(i+1, k-1, u, t) * (t[i+k] - u)
        
        if expr1 == 0 or d1 == 0:
            term1 = 0.0
        else:
            term1 = float(expr1) / d1
        
        if expr2 == 0 or d2 == 0:
            term2 = 0.0
        else:
            term2 = float(expr2) / d2
        
        
        return term1 + term2

def Q(u, controlPoints, knots, mode):

    n = len(controlPoints)
    k = len(knots) - n 
    
    if mode:
        result = np.array([0, 0], dtype=float)
        for i in xrange(n):
            result += controlPoints[i] * N(i, k, u, knots)
        return result


    # different de Boor algorithm, somehow doesn't work too well though >_>
    # handle edge cases for speed by hand
    k = k - 1
    if np.allclose(u, knots[n + k - 1], 1e-20):
        return controlPoints[n - 1]
    if np.allclose(u, knots[0], 1e-20):
        return controlPoints[0]
    s = 0
    knot_len = len(knots)
    while s < knot_len - 1:
        s += 1
        if u < knots[s]:
            s = s - 1
            break
    # count multiplicity
    mu = 0
    for elem in knots:
        if elem == u:
            mu += 1
            
    # calculate the coordinate
    r = np.empty([k - mu + 1, 2])
    for l in range(k - mu + 1):
        r[l] = controlPoints[s - k + l]
    
    for j in xrange(1, k - mu + 1):
        for l in xrange(0, k - mu - j + 1):
            alpha = (u - knots[s - k + j + l]) / (knots[s+j+l] - knots[s-k+j+l])
            r[l] = (1 - alpha) * r[l] + alpha * r[l+1]
    return r[0]


def insertKnot(tp, knots, controlPoints):
    # number of control points
    n = len(controlPoints)
    k = len(knots) - n
    new = np.empty([n+1, 2], dtype = np.float)
    j = -1
    # insert the new knot
    knot_len = len(knots)
    while j < knot_len - 1:
        j += 1
        if tp < knots[j]:
            knots = np.insert(knots, j, [tp])
            j = j - 1
            break
    r = 0
    for elem in knots:
        if np.allclose(tp, elem):
            r += 1
    n = n - 1    
    new[0] = controlPoints[0]
    new[n+1] = controlPoints[n]
    for i in xrange(1, n + 1):
        ai = a(tp, j, i, knots, k, n)
        new[i] = (1 - ai) * controlPoints[i - 1] + ai * controlPoints[i]
        
    controlPoints = new
    return knots, controlPoints
    
    
def a(tp, j, i, t, k, n):
    if 1 <= i and i <= (j - k):
        return 1
    elif (j - k + 1) <= i and i <= j:
        expr = (tp - t[i]) / (t[i+k] - t[i])
        return expr
    elif i <= n:
        return 0