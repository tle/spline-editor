from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
import sys
import numpy as np

from splinemath import * 

global controlPoints
global knots
global left_down, right_down, mouse_x, mouse_y, clicked_index
global control_polyline, dt, old_dt, antialiasing, mode, filename
filename = None
mode = 1
antialiasing = 1
control_polyline = 1
left_down = 0
right_down = 0
clicked_index = None

knots = np.array([0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0], dtype = np.float)
controlPoints = np.array([[-1.0, 1.0], [-1.0, -1.0], [1.0, -1.0], [1.0, 1.0]],
                            dtype = np.float)
                            


controlPoints *= 0.6

# drawing resolution
dt = np.float(0.01)
old_dt = dt

renderedPoints = np.empty([int(1/dt), 2], dtype = np.float)
renderedPoints = []

def redraw():
    global control_polyline
    global controlpoints, knots, dt, mode
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    
    if control_polyline == 1:
        # start drawing here
        # draw the control points
        glBegin(GL_POINTS)
        glColor4f(1.0, 1.0, 1.0, 1.0)
        for i in xrange(len(controlPoints)):
            points = controlPoints[i]
            glVertex2f(points[0], points[1])    
        glEnd()

        # draw lines between the control points
        glBegin(GL_LINES)
        glColor4f(1.0, 1.0, 1.0, 0.9)
        last = controlPoints[0]
        for i in xrange(1, len(controlPoints), 1):
            point = controlPoints[i]
            glVertex2f(last[0], last[1])
            glVertex2f(point[0], point[1])
            last = point
        glEnd()
    
    # draw the curve
    u = 0.0
    glBegin(GL_LINE_STRIP)
    glColor4f(1.0, 0.0, 0.0, 0.5)
    last = Q(u, controlPoints, knots, mode)
    while np.less(u, 1.0):
        #print u
        renderedPoints.append([last, u])
        ut = old_dt
        point = Q(u + ut, controlPoints, knots, mode)
        diff = last - point
        while np.linalg.norm(diff) > ut / 2:
            ut = ut / 2
            point = Q(u + ut, controlPoints, knots, mode)
            old_diff = diff
            diff = last - point
            # calculate improvement over last iteration
            improv = np.linalg.norm(diff - old_diff)
            if improv < old_dt / 2:
                break
        u = u + old_dt
        last = point
        glVertex2f(last[0], last[1])
    glEnd()
    glutSwapBuffers();

def resize(w, h):
    if (h ==0):
        h = 1

    if (w > h):
        w = h
    else:
        h = w

    glViewport(0, 0, w, h)
    
    glutPostRedisplay()
    
def keyfunc(*args):
    global control_polyline, left_down, antialiasing, mode
    global controlPoints, knots, filename
    key = args[0]
    if (key == '\033' or key == 'q' or key == 'Q'):
        sys.exit(0)
    elif key == 'c':
        control_polyline = control_polyline ^ 1
        left_down = 0
        glutPostRedisplay()
    elif key == 'm':
        mode = mode ^ 1
        glutPostRedisplay()
    elif key == 'a':
        antialiasing = antialiasing ^ 1
        if antialiasing:
            #print 'antialias on'
            glEnable(GL_POINT_SMOOTH)
            glEnable(GL_LINE_SMOOTH)
        else:
            #print 'antialias off'
            glDisable(GL_POINT_SMOOTH)
            glDisable(GL_LINE_SMOOTH)
        glutPostRedisplay()
    elif key == 's':
        if filename == None:
            #print 'Defaulting to nurbs.nb.'
            filename = 'nurbs.nb'
        f = open(filename, 'w')
        #print str(controlPoints)
        #print knots.T
        k = np.matrix([knots, knots]).T
        index = len(controlPoints)
        header = np.matrix([index, 0])
        pack = np.vstack((header, controlPoints, k))
        np.save(f, pack)
        glutPostRedisplay()
    elif key == 'l':
        if filename == None:
            #print 'Defaulting to nurbs.nb.'
            filename = 'nurbs.nb'
        try:
            f = open(filename, 'r')
            pack = np.load(f)
            index = int(pack[0,0]) + 1
            controlPoints = np.array(pack[1:index])
            knots = np.array(pack[index:, 0])
            #print pack
            #print controlPoints
            #print knots
            glutPostRedisplay()
            f.close()
        except:
            print 'File', filename, 'doesn\'t exist yet'
    
    
def mouse(button, state, x, y):
    global model, proj, view
    global left_down, right_down
    global mouse_x, mouse_y
    global controlPoints
    global clicked_index
    global knots
     
    if button == GLUT_LEFT_BUTTON and state == GLUT_DOWN and control_polyline == 1:
        left_down = 1
        mouse_x = x
        mouse_y = y
        
        wc_x = (x / float(window_width) - 0.5) * 2
        wc_y = (0.5 - y / float(window_height)) * 2
        
        # find any control point in vicinity
        
        for i in xrange(len(controlPoints)):
            cp = controlPoints[i]
            dx = wc_x - cp[0]
            dy = wc_y - cp[1]
            norm = np.linalg.norm([dx, dy])
            if norm < 0.1:
                clicked_index = i
                break
                
                
        
    
    if button == GLUT_LEFT_BUTTON and state == GLUT_UP and control_polyline == 1:
        clicked_index = None
        left_down = 0
        glutPostRedisplay()
    
    if button == GLUT_RIGHT_BUTTON and state == GLUT_DOWN:
        right_down = 1
        wc_x = (x / float(window_width) - 0.5) * 2
        wc_y = (0.5 - y / float(window_height)) * 2
        dist = None
        for i in xrange(len(renderedPoints)):
            rp = renderedPoints[i][0]
            u = renderedPoints[i][1]
            dx = wc_x - rp[0]
            dy = wc_y - rp[1]
            norm = np.linalg.norm([dx, dy])
            if norm < 0.1:
                dist = u
                break
        if dist:
            #print dist
            k, cp = insertKnot(dist, knots, controlPoints)
            knots = k
            controlPoints = cp
            glutPostRedisplay()
            
    if button == GLUT_RIGHT_BUTTON and state == GLUT_UP:
        right_down = 0
        
def motion(x, y):
    global model, proj, view
    global left_down, clicked_index
    global controlPoints, mouse_x, mouse_y
    if left_down == 1 and control_polyline == 1:
        dx = (mouse_x - x) / float(window_width) * 2
        dx = -dx
        dy = (mouse_y - y) / float(window_height) * 2
        if left_down and clicked_index != None:
            cp_x = (x / float(window_width) - 1.0) * 2
            cp_y = (1.0 - y / float(window_height)) * 2
            controlPoints[clicked_index] += [dx, dy] 
            glutPostRedisplay()
        mouse_x = x
        mouse_y = y
    
def InitGL():    
    glEnable(GL_CULL_FACE)
    glCullFace(GL_BACK)

    glEnable(GL_DEPTH_TEST)
    
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    
    # enable transparency
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    glBlendEquation(GL_MAX)
    glEnable(GL_BLEND)
    glClearColor(0.0,0.0,0.0,0.0)
    glClearDepth(1.0)
    
    if antialiasing:
        # anti-aliasing
        glEnable(GL_POINT_SMOOTH)
        glEnable(GL_LINE_SMOOTH)
    
    # point and line width
    glPointSize(10)
    glLineWidth(2)

    # create a load/save menu
    
    
    glDisable(GL_LIGHTING)

    
def main():
    global window_width, window_height, filename
    glutInit(sys.argv)
    
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH)

    args = sys.argv[1:]

    xres = int(args[0])
    yres = int(args[1])
    if len(args) > 2:
        filename = args[2]

    window_width = xres
    window_height = yres
    
    glutInitWindowSize(window_width, window_height)
    glutInitWindowPosition(300, 100)

    glutCreateWindow("CS171 HW0")
    
    InitGL()

    glutDisplayFunc(redraw)
    glutReshapeFunc(resize)
    glutKeyboardFunc(keyfunc)
    glutMouseFunc(mouse)
    glutMotionFunc(motion)

    glutMainLoop()

main()
